# Vacuum Pump Controller Pfeiffer TCP 350

EPICS module to provide communications and read/write data from/to **Pfeiffer TCP350 vacuum pump controllers** and pumps

**Please note that the pump has to be configured together with the controller i.e. in the same IOC**

There are separate `.iocsh` scripts for the controller and the pump.

## IOCSH files

*   Controller: [vac_ctrl_tcp350_moxa.iocsh](iocsh/README.md#vac_ctrl_tcp350_moxa)
*   Pump: [vac_pump_tcp350_vpt.iocsh](iocsh/README.md#vac_pump_tcp350_vpt)


## MOXA configuration

#### Operation Modes / Operating settings

```
Application: Socket
Mode: TCP Server
```

OR

```
Operation mode: TCP Server mode
```

```
.
.
.
Packing length: 0
Delimiter 1: 00 / Disabled
Delimiter 2: 00 / Disabled
Delimiter process: Do Nothing
Force transmit: 0
```

#### Communication Parameters / Serial Settings

```
Baud rate: 9600
Data bits: 8
Stop bits: 1
Parity: None
Flow control: None
Interface: RS-485 2-wire
```
