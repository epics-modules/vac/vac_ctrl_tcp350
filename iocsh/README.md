# IOCSH files for Pfeiffer TCP350 turbo pump controller and its pump

## vac_ctrl_tcp350_moxa

`.iocsh` file for the controller.

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **IPADDR**
    *   The IP address or fully qualified domain name of the Moxa serial ethernet controller the pump controller is connected to
    *   string
*   **PORT**
    *   The TCP port number of the serial port on the Moxa where the pump controller is connected to (serial port 1 is usually TCP port 4001, serial port 2 is TCP port 4002, and so on)
    *   integer

#### Optional macros:

*   **SERIAL_ADDRESS**
    *   RS485 address of device. Possible values: 1-255
    *   default is `1`
    *   integer


## vac_pump_tcp350_vpt

`.iocsh` file for the pump itself

#### Required macros:

*   **DEVICENAME**
    *   ESS name, the same as in Naming service/CCDB
    *   string
*   **CONTROLLERNAME**
    *   The ESS name of the pump controller the pump is connected to
    *   string
