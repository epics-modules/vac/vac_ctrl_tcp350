#include <stdio.h>
#include "StreamError.h"
#include "StreamFormatConverter.h"


/*
 * StreamDevice format converter for EPICS support for the Pfeiffer Vacuum TCP350 controller
 * The telegram frame has a length field, this class provides a %P conversion character followed
 * by the code of the Pfeiffer telegram data type to encode and decode the len+data part of
 * messages conforming to the Pfeiffer Vacuum protocol.
 *
 * Author: Krisztian Loki
 */


#define LOG_PREFIX "TCP350Format: "

class TCP350Format : public StreamFormatConverter
{
	friend class StreamFormatConverterRegistrar<TCP350Format>;
private:
	TCP350Format() {}
	TCP350Format(const TCP350Format&);

	int scanLen(const char *input);

	enum DataType { boolean_old = '0',
			u_integer   = '1',
			u_real      = '2',
			string      = '4',
			boolean_new = '6',
			u_short_int = '7'};

public:
	int  parse(const StreamFormat &fmt, StreamBuffer &info, const char*& source, bool scanFormat) override;

	bool    printDouble(const StreamFormat &fmt, StreamBuffer &output, double value) override;
	ssize_t scanDouble(const StreamFormat &fmt, const char* input, double &value) override;

	bool    printLong(const StreamFormat &fmt, StreamBuffer &output, long value) override;
	ssize_t scanLong(const StreamFormat &fmt, const char* input, long &value) override;

	bool    printString(const StreamFormat &fmt, StreamBuffer &output, const char* value) override;
	ssize_t scanString(const StreamFormat &fmt, const char* input, char* value, size_t &size) override;
};


int TCP350Format::parse(const StreamFormat &fmt, StreamBuffer &info, const char*& source, bool scanFormat)
{
	StreamFormatType type;

	info.append(source[0]);
	switch (source[0])
	{
		// Boolean - old
		case boolean_old:
			type = unsigned_format;
			if (!scanFormat)
				info.append("06%.6d");
			break;

		// Positive integer number
		case u_integer:
			type = unsigned_format;
			if (!scanFormat)
				info.append("06%.6d");
			break;

		// Positive fixed point number
		case u_real:
			type = double_format;
			if (!scanFormat)
				info.append("06%06.f");
			break;

		// String
		case string:
			type = string_format;
			if (!scanFormat)
				info.append("xx%s");
			break;

		// Boolean - new
		case boolean_new:
			type = unsigned_format;
			if (!scanFormat)
				info.append("01%.1d");
			break;

		// Positive integer number
		case u_short_int:
			type = unsigned_format;
			if (!scanFormat)
				info.append("03%.3d");
			break;

		default:
			error(LOG_PREFIX "Data type %c is not supported\n", source[0]);
			return false;
	}

	++source;
	return type;
}


int TCP350Format::scanLen(const char *input)
{
	int len = -1;
	int slen = strlen(input);

	if (slen < 2 || sscanf(input, "%2d", &len) != 1 || len < 0 || len > 99)
	{
		error(LOG_PREFIX "Cannot scan length in '%s'\n", input);
		return -1;
	}

	if (len > slen - 2)
	{
		error(LOG_PREFIX "Data too short\n");
		return -1;
	}

	return len;
}


bool TCP350Format::printDouble(const StreamFormat &fmt, StreamBuffer &output, double value)
{
	switch (fmt.info[0])
	{
		case u_real:
#ifdef U_REAL_CAST_TO_INT
			value = (int)(value * 100.0);
#else
			value *= 100.0;
#endif
			break;

		default:
			error(LOG_PREFIX "printDouble is not defined for data type %c\n", fmt.info[0]);
			return false;
	}

	output.print(fmt.info + 1, value);

	return true;
}


ssize_t TCP350Format::scanDouble(const StreamFormat &fmt, const char* input, double &value)
{
	if (fmt.info[0] != u_real)
	{
		error(LOG_PREFIX "scanDouble is not defined for data type %c\n", fmt.info[0]);
		return -1;
	}

	int len = scanLen(input);

	if (len == -1)
		return -1;

	if (len != 6)
	{
		error(LOG_PREFIX "Invalid length: %d\n", len);
		return -1;
	}

	long lvalue;
	if (sscanf(input + 2, "%6ld", &lvalue) != 1)
	{
		error(LOG_PREFIX "Cannot parse double\n");
		return -1;
	}

	value = (double)lvalue / 100.0;

	return 2 + len;
}


bool TCP350Format::printLong(const StreamFormat &fmt, StreamBuffer &output, long value)
{
	switch (fmt.info[0])
	{
		case boolean_old:
			value *= 111111;
			break;

		case u_integer:
		case u_short_int:
			break;

		case boolean_new:
			value = !!value;
			break;

		default:
			error(LOG_PREFIX "printLong is not defined for data type %c\n", fmt.info[0]);
			return false;
	}

	output.print(fmt.info + 1, value);

	return true;
}


ssize_t TCP350Format::scanLong(const StreamFormat &fmt, const char* input, long &value)
{
	int len = scanLen(input);

	if (len == -1)
		return -1;

	if (len != 1 && len != 3 && len != 6)
	{
		error(LOG_PREFIX "Invalid length: %d\n", len);
		return -1;
	}

	char format[] = "%.ld";
	format[1] = len + '0';

	if (sscanf(input + 2, format, &value) != 1)
	{
		error(LOG_PREFIX "Cannot parse long\n");
		return -1;
	}

	switch (fmt.info[0])
	{
		case boolean_old:
			if (value != 0 && value != 111111)
			{
				error(LOG_PREFIX "Invalid boolean value %ld\n", value);
				return -1;
			}
			value = !!value;
			break;

		case boolean_new:
			if (value != 0 && value != 1)
			{
				error(LOG_PREFIX "Invalid boolean value %ld\n", value);
				return -1;
			}
			break;

		case u_integer:
		case u_short_int:
			break;

		default:
			error(LOG_PREFIX "scanLong is not defined for data type %c\n", fmt.info[0]);
			return -1;
	}

	return 2 + len;
}



bool TCP350Format::printString(const StreamFormat &fmt, StreamBuffer &output, const char* value)
{
	output.print(fmt.info + 1, value);

	// Do not include the xx prefix
	int len = output.length() - 2;
	char lens[3];

	if (snprintf(lens, 3, "%.2d", len) != 2)
	{
		return false;
	}

	output[0] = lens[0];
	output[1] = lens[1];

	return true;
}


ssize_t TCP350Format::scanString(const StreamFormat &fmt, const char* input, char* value, size_t &size)
{
	if (fmt.info[0] != string)
	{
		error(LOG_PREFIX "scanString is not defined for data type %c\n", fmt.info[0]);
		return -1;
	}

	int len = scanLen(input);

	if (len == -1)
		return -1;

	if ((size_t)len > size)
	{
		error(LOG_PREFIX "Refusing to parse %d byte long string, maximum allowed is %lu\n", len, size);
		return -1;
	}

	char format[5];

	if (snprintf(format, 5, "%%%02dc", len) > 4)
	{
		error(LOG_PREFIX "Cannot construct scanf format\n");
		return -1;
	}

	if (sscanf(input + 2, format, value) != 1)
	{
		error(LOG_PREFIX "Cannot parse string\n");
		return -1;
	}

	size = strnlen(value, size);

	return 2 + len;
}


RegisterConverter (TCP350Format, "P");
